package model;

public class MatrixModel {
	public String id;
//	public float positionSize1;
//	public float positionSize2;
//	public float positionSize3;
//	
//	public float profitTargetPercentage1;
//	public float profitTargetPercentage2;
//	public float profitTargetPercentage3;

	public float[] profitTargetPercentage = {0.0f,0.0f,0};
	public float[] profitTargetLotPercentage = {0.0f,0.00f,0.0f};	

	public float getProfitTargetPercentage(int i) {
		return profitTargetPercentage[i];
	}
	public void setProfitTargetPercentage(int i, float c) {
		this.profitTargetPercentage[i] = c;
	}
	public float getProfitTargetLotPercentage(int i) {
		return profitTargetLotPercentage[i];
	}
	public void setProfitTargetLotPercentage(int i, float c) {
		this.profitTargetLotPercentage[i] = c;
	}
	
	
}
