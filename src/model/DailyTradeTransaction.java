package model;

public class DailyTradeTransaction {

	
	public float entry;
	public String entryTime;
	public String exitTime;
	public float stopLoss;
	public float profitTarget;
	public float profitTartgetPercentage;
	public float lotSize;
	public float lotSizePercentage;
	public String type;
	public Boolean isEntryPriceGotHit;
	public Boolean isStopLossGotHit;
	public Boolean isProfitTargetHit;
	public Boolean active;
	public float profit;
}
