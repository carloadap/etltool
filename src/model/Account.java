package model;

import java.util.LinkedList;
import java.util.List;

import manager.AppProperties;

public class Account {
	public float balance;
	public float riskPercentage;
	public float averageStopLoss;
	public float baseCapital;
	public float[] profitTargetPercentage = {0.6f,0.8f,1};
	public float[] profitTargetLotPercentage = {0.25f,0.50f,0.25f};	
	public List<DailyTradeTransaction> buyList = new LinkedList<DailyTradeTransaction>();	
	public List<DailyTradeTransaction> sellList = new LinkedList<DailyTradeTransaction>();
	public void setProfitTargetPercentage(float[] profitTargetPercentage) {
		this.profitTargetPercentage = profitTargetPercentage;
	}
	public float getProfitTargetPercentage(int i) {
		return profitTargetPercentage[i];
	}
	public void setProfitTargetPercentage(int i, float c) {
		this.profitTargetPercentage[i] = c;
	}
	
	public float getProfitTargetLotPercentage(int i) {
		return profitTargetLotPercentage[i];
	}
	public void setProfitTargetLotPercentage(int i, float c) {
		this.profitTargetLotPercentage[i] = c;
	}
	
	
}
