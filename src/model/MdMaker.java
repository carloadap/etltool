package model;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import manager.ExtractTransformLoadManager;
import manager.PositionManager;

public class MdMaker {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		//createMatrix();
		transform();
	}

	private static void transform() {
		ExtractTransformLoadManager  m = null;
		m = new ExtractTransformLoadManager();
		Hashtable<String, Object> t = m.transform();
		Trade trade = (Trade) t.get("2012.04.3017:00");
		System.out.println(trade.close);
		System.out.println("End");
		
		int year;
		int month;
		int day;
		int hour;
		
		for (Map.Entry<String,Object> entry : t.entrySet()) {
		    System.out.println(entry.getKey());
		}
		
	}

	private static void createMatrix() throws IOException {
		System.out.println("Create the Profit matrix model");
		//
		//pt1
		FileWriter writer = new FileWriter("/Users/clydemoreno/Downloads/nadex/ProfitMatrixModel.csv");
		
		
		PositionManager pm = new PositionManager();
		Position p;
		float size2 = 0;
		ArrayList<Position> list = new ArrayList<Position>();
		int id = 0;
		List<Position> ll = new LinkedList<Position>();
		for(int i = 0; i <= 100;i+=2){
			for(int j = 0; j <= 100; j+=2){
				for(int k = 0; k <= 100; k+=2){
					if(i + j + k == 100){
						for(int l = 0; l <= 100;l+=3){
							for(int m = 50; m <= 150;m+=5){
								
								for(int n = 80; n <= 200; n+=10){
								p = new Position();
								//ll.add(p);
								p.id = id++;
								p.positionSize1 = i;
								p.positionSize2 = j;
								p.positionSize3 = k;
								
								p.profitTargetPercentage1 = l;
								p.profitTargetPercentage2 = m;
								p.profitTargetPercentage3 = n;
//								if(m > l){
//									p.profitTargetPercentage2 = m;
//								}
//								else{
//									p.profitTargetPercentage2 = l;
//								}
//								if(n > m){
//								p.profitTargetPercentage3 = n;
//								}
//								else{
//									p.profitTargetPercentage3 = l;
//								}
								
//								if(l == 0){
//									p.profitTargetPercentage1 = m;
//								}
//								else if(l == 1){
//									p.profitTargetPercentage2 = m;
//								}
//								else if(l == 2){
//									p.profitTargetPercentage3 = m;
//								}
								
								writer.append(pm.toString(p));
								//pm.display(p);
								
								}
							}
						}
					}
				}
			}
				
		}
		//Position pp = list.get(50);
		System.out.println(String.format("Done. Total items: %d\n", id));
//		for(int i = 100;i > 0;i--){
//			//for(int j = 100; j >= 0;j--){
//			    size2 = 100 - i;
//				for(int j = 0; j < size2;size2++){
//					p = new Position();
//					p.positionSize1 = i;
//					p.positionSize2 = j;
//					size2 = p.positionSize2;
//					p.positionSize3 = 100 - (p.positionSize1 + p.positionSize2);
//					pm.display(p);
//				}
//				//p.positionSize3 = 100 - (p.positionSize2 + p.positionSize1);
//								
//			//}
//
//		}
	}

}
