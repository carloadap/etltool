package utility;

import model.DailyTradeTransaction;

public class Calculator {
	public static float getProfitTarget(String type, float high, float low, float percentage)
	{
		if(type=="Buy")	
			return high+((high-low)*percentage);
		else	
			return low-((high-low)*percentage);
	}
	public static float getStopLoss(float high, float low)
	{
		return high-((high-low)/2);
	}
	public static float calculateProfit(DailyTradeTransaction buyDailyTradeTransaction) {
		return ((buyDailyTradeTransaction.stopLoss-buyDailyTradeTransaction.entry))*buyDailyTradeTransaction.lotSize;
	}	
}
