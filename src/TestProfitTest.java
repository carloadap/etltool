import static org.junit.Assert.*;

import manager.AccountManager;
import model.Account;

import org.junit.Assert;
import org.junit.Test;

import utility.Calculator;


public class TestProfitTest {

	@SuppressWarnings("deprecation")
	@Test
	public final void testProfitTargetOnBuy() {
		float profitTarget = Calculator.getProfitTarget("Buy", 50.0f, 10.0f, 0.50f);
		System.out.printf("buy target: %s\n", profitTarget);
		
		Assert.assertTrue(profitTarget == 70.0f);
	}
	@Test
	public final void testProfitTargetOnSell() {
		float profitTarget = Calculator.getProfitTarget("Sell", 50.0f, 10.0f, 0.50f);
		System.out.printf("sell target: %s\n", profitTarget);
		
		Assert.assertTrue(profitTarget==-10.0f);
	}
	@Test
	public final void testCalculationStopLoss() {
		float stopLoss = Calculator.getStopLoss(50.0f, 10.0f);
		System.out.printf("stop loss: %s\n", stopLoss);
		
		Assert.assertTrue(stopLoss == 30.0f);
	}
		
	@Test
	public final void testCalculateLotSize() {
		AccountManager m = new AccountManager();
		Account a = new Account();
		a.balance = 8000f;
		a.riskPercentage = 0.03f;
		a.averageStopLoss = 4.0f;
		float lotSize = m.getLotSize(a);
		
		
		System.out.printf("lot size: %s\n", lotSize);
		
		Assert.assertTrue(Math.round(lotSize) == 60.0f);
	}
}
