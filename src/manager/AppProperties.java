package manager;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 */
public class AppProperties {

    private Properties prop = new Properties();
    private static AppProperties instance;

    public static AppProperties getInstance() {
        if (instance == null) {
            instance = new AppProperties();
        }
        return instance;
    }

    public void load() {
        try {
            //load from file
//            prop.load(ForexNotification.class.getClassLoader().getResourceAsStream("App.properties"));
            prop.load(new FileInputStream("App.properties"));
        } catch (IOException ex) {
            Logger.getLogger(AppProperties.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getProperty(String propertyName) {
        return prop.getProperty(propertyName);
    }

    public void setProperty(String propertyName, String propertyValue) {

        try {
            prop.setProperty(propertyName, propertyValue);
            prop.store(new FileWriter("App.properties"), null);
        } catch (IOException ex) {
            Logger.getLogger(AppProperties.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}