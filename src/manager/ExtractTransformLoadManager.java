package manager;

import java.util.Hashtable;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.MatrixModel;
import model.Trade;

public class ExtractTransformLoadManager {
	public Hashtable<String, MatrixModel> extractModel(){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String csvFile = appProperties.getProperty("matrixModelFileName");
        String startProcessingIndexOfModel = appProperties.getProperty("startProcessingIndexOfModel");
        String endProcessingIndexOfModel = appProperties.getProperty("endProcessingIndexOfModel");
    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";
    	Hashtable<String, MatrixModel> table = null;
		table = new Hashtable<String, MatrixModel>();
		//date, time, open, high, low, close, volume
		//2012.01.06,22:00,1617.02000,1617.02000,1615.50000,1616.27000,676
		MatrixModel model = null;
		boolean isGoodToGo = true;
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] lineModels = line.split(cvsSplitBy);
				if(lineModels.length == 7){
					
    				//THIS WILL CODE WILL STOP READING THE FILE WHEN IT FINDS THE END OF PROCESSING DATE
					
//    				if(Integer.parseInt(lineModels[0]) >= Integer.parseInt(startProcessingIndexOfModel) 
//    						&& Integer.parseInt(lineModels[0]) <= Integer.parseInt(endProcessingIndexOfModel)){
//       					System.out.print("this should  show up once\n");
//    					isGoodToGo = true;
//    				}					
//    				else{
//    					isGoodToGo = false;
//    				}
    				
					//end of the index
					isGoodToGo = Integer.parseInt(lineModels[0]) <= Integer.parseInt(endProcessingIndexOfModel);
					
					if(!isGoodToGo){
						break;
					}
					
					//ignore until we get to the starting index
					if(Integer.parseInt(lineModels[0]) >= Integer.parseInt(startProcessingIndexOfModel)){
						model = addToHashtable(table, lineModels);
					}
				}
			}		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;
	}
	private MatrixModel addToHashtable(Hashtable<String, MatrixModel> table,
			String[] lineModels) {
		MatrixModel model;
		model = new MatrixModel();
		model.id = lineModels[0];
		model.setProfitTargetLotPercentage(0, Float.parseFloat(lineModels[1]));
		model.setProfitTargetLotPercentage(1, Float.parseFloat(lineModels[2]));
		model.setProfitTargetLotPercentage(2, Float.parseFloat(lineModels[3]));
		model.setProfitTargetPercentage(0,Float.parseFloat(lineModels[5]));
		model.setProfitTargetPercentage(1,Float.parseFloat(lineModels[6]));
		model.setProfitTargetPercentage(2,Float.parseFloat(lineModels[4]));
		table.put(model.id, model);
		return model;
	}
    public Hashtable<String, Trade> transform(){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String csvFile = appProperties.getProperty("filePath");
    	//String csvFile = "/Users/clydemoreno/Downloads/nadex/gold2007-1-18.csv";
    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";
    	Hashtable<String, Trade> table = null;
    	try {
    		table = new Hashtable<String, Trade>();
    		//date, time, open, high, low, close, volume
    		//2012.01.06,22:00,1617.02000,1617.02000,1615.50000,1616.27000,676
    		br = new BufferedReader(new FileReader(csvFile));
    		while ((line = br.readLine()) != null) {
     
    		        // use comma as separator
    			String[] trades = line.split(cvsSplitBy);
    			//System.out.println(String.format("Date: %s Time: %s ",trades[0], trades[1]));
     
    			
    			if(trades.length == 7){
    				Trade t = new Trade();
    				t.id = String.format("%s,%s",trades[0], trades[1]);
    				t.open = Float.parseFloat(trades[2]);
    				t.high = Float.parseFloat(trades[3]);
    				t.low = Float.parseFloat(trades[4]);
    				t.close = Float.parseFloat(trades[5]);
    				
    				//possibly check if within the months stated
    				//System.out.print(t.id.substring(0,7).equals("2007.05"));
    				
    				String endProcessingDate = appProperties.getProperty("endProcessingDate");

    				//THIS WILL CODE WILL STOP READING THE FILE WHEN IT FINDS THE END OF PROCESSING DATE
    				if(t.id.substring(0,7).equals(endProcessingDate)){
//       					System.out.print("this should  show up once\n");
    					break;
    				}
   				    table.put(t.id,t );
    				
    			}
    			
    		}
     
    	} catch (FileNotFoundException e) {
    		e.printStackTrace();
    	} catch (IOException e) {
    		e.printStackTrace();
    	} finally {
    		if (br != null) {
    			try {
    				br.close();
    			} catch (IOException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	return table;
    }
}
