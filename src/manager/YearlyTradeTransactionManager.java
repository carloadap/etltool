package manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;

import model.MatrixModel;
import model.Trade;
import model.YearlyTradeTransaction;

public class YearlyTradeTransactionManager {
	public void logToFile(){
		
	}
	public void processDailyTradeTransactions(Hashtable<String, Trade>t){
		
	}
	public Hashtable<String, YearlyTradeTransaction> extractYearlyOutput(){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String csvFile = appProperties.getProperty("outputFilePath");
        String processSummaryOfYear = appProperties.getProperty("processSummaryOfYear");
        
    	BufferedReader br = null;
    	String line = "";
    	String cvsSplitBy = ",";
    	Hashtable<String, YearlyTradeTransaction> table = null;
		table = new Hashtable<String, YearlyTradeTransaction>();
		//date, time, open, high, low, close, volume
		//2012.01.06,22:00,1617.02000,1617.02000,1615.50000,1616.27000,676
		YearlyTradeTransaction yearlyTrans = null;
			System.out.print("Summarizing the yearly reports...\n");
			 		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				String[] lineModels = line.split(cvsSplitBy);
				if(lineModels.length == 11){
    				if(lineModels[0].substring(0,4).equals(processSummaryOfYear)){
       					//System.out.print("Once the year is seen, it will stop the reading of file\n");
    					break;
    				}			
    				//todo: 
    				//change the hard coded values
    				yearlyTrans = (YearlyTradeTransaction)table.get(String.format("%s-%s",lineModels[0].substring(0,4),lineModels[10]));
    				if(yearlyTrans == null){
    					yearlyTrans = new YearlyTradeTransaction();
    					yearlyTrans.id = lineModels[10];
    					yearlyTrans.year = Integer.parseInt(lineModels[0].substring(0,4));
    				}
    				yearlyTrans.profit += Float.parseFloat(lineModels[3]);
					//add to hashtable
    				table.put(String.format("%s-%s",yearlyTrans.year,yearlyTrans.id), yearlyTrans);
				}
			}	
			
			//System.out.println(String.format("Year 2007 profit:$%f",yearlyTrans.profit));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return table;
	}

	
}
