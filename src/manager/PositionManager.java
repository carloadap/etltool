package manager;

import model.Position;

public class PositionManager {
	public void display(Position p){
		System.out.print(String.format("%s,%f,%f,%f,%f,%f,%f\n"
				, p.id,p.positionSize1,p.positionSize2,p.positionSize3,p.profitTargetPercentage1,p.profitTargetPercentage2,p.profitTargetPercentage3));
	}
	
	public String toString(Position p){
		return String.format("%s,%f,%f,%f,%f,%f,%f\n"
				, p.id,p.positionSize1,p.positionSize2,p.positionSize3,p.profitTargetPercentage1,p.profitTargetPercentage2,p.profitTargetPercentage3);
	}
}
