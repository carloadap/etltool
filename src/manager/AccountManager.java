package manager;

import model.Account;

public class AccountManager {
	
	private static AccountManager instance;
	
	public static AccountManager getInstance()
	{
		if(instance==null)
		{
			instance=new AccountManager();
		}
		return instance;
	}
	
	public float getLotSize(Account account)
	{
		float lots;
		account.baseCapital = account.averageStopLoss/account.riskPercentage;
		lots = ((account.balance-account.baseCapital)/account.baseCapital)+1;
		//averagestoploss/riskpercentage = basecapital
		//balance-basecapital=multiplier
		//(multiplier/basecapital)+1 = lots
		return lots;
	}
	
}
