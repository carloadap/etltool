package manager;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import model.Position;

public class MatrixModelMaker {
	public static void createMatrix() throws IOException {
		System.out.println("Create the Profit matrix model");

		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        String matrixModelFileName = appProperties.getProperty("matrixModelFileName");

		FileWriter writer = new FileWriter(matrixModelFileName);
		
		
		PositionManager pm = new PositionManager();
		Position p;
		float size2 = 0;
		ArrayList<Position> list = new ArrayList<Position>();
		int id = 0;
		List<Position> ll = new LinkedList<Position>();
		for(int i = 20; i <= 100;i+=10){
			for(int j = 20; j <= 100; j+=10){
				for(int k = 20; k <= 100; k+=10){
					if(i + j + k == 100){
						//profit percentage allocator
						for(int l = 60; l <= 100;l+=10){
							for(int m = 70; m <= 150;m+=10){
								
								for(int n = 80; n <= 200; n+=10){
								p = new Position();
								//ll.add(p);
								p.id = id++;
								//change to decimal
								p.positionSize1 = i/100.00f;
								p.positionSize2 = j/100.00f;
								p.positionSize3 = k/100.00f;
								
								p.profitTargetPercentage1 = l/100.00f;
								p.profitTargetPercentage2 = m/100.00f;
								p.profitTargetPercentage3 = n/100.00f;
//								if(m > l){
//									p.profitTargetPercentage2 = m;
//								}
//								else{
//									p.profitTargetPercentage2 = l;
//								}
//								if(n > m){
//								p.profitTargetPercentage3 = n;
//								}
//								else{
//									p.profitTargetPercentage3 = l;
//								}
								
//								if(l == 0){
//									p.profitTargetPercentage1 = m;
//								}
//								else if(l == 1){
//									p.profitTargetPercentage2 = m;
//								}
//								else if(l == 2){
//									p.profitTargetPercentage3 = m;
//								}
								
								writer.append(pm.toString(p));
								//pm.display(p);
								
								}
							}
						}
					}
				}
			}
				
		}
		//Position pp = list.get(50);
//		System.out.println(String.format("Done. Total items: %d\n", id));
//		for(int i = 100;i > 0;i--){
//			//for(int j = 100; j >= 0;j--){
//			    size2 = 100 - i;
//				for(int j = 0; j < size2;size2++){
//					p = new Position();
//					p.positionSize1 = i;
//					p.positionSize2 = j;
//					size2 = p.positionSize2;
//					p.positionSize3 = 100 - (p.positionSize1 + p.positionSize2);
//					pm.display(p);
//				}
//				//p.positionSize3 = 100 - (p.positionSize2 + p.positionSize1);
//								
//			//}
//
//		}
	}

}
