package manager;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import com.sun.xml.internal.ws.util.StringUtils;

import model.MatrixModel;
import model.Position;
import model.Trade;
import model.YearlyTradeTransaction;
public class MdMaker {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		   Calendar cal = Calendar.getInstance();
		   System.out.printf("Start time: %s\n",dateFormat.format(cal.getTime()));

//		   MatrixModelMaker.createMatrix();
		   
		   LogManager.clearLogs("outputFilePath");
		   LogManager.clearLogs("yearlyMatrixModelOutput");
		   transform();

		   System.out.printf("End time: %s\n",dateFormat.format(cal.getTime()));

	}
	private static void processYearlyTransactions() {
		YearlyTradeTransactionManager mg = new YearlyTradeTransactionManager();
		Hashtable<String,YearlyTradeTransaction> trans = mg.extractYearlyOutput();
		
		 Set<String> keys = trans.keySet();
		 int i = 1;
	        for(String key: keys){
	        	//
	        	YearlyTradeTransaction yearlyTrans = (YearlyTradeTransaction)trans.get(key);
	            //System.out.println(String.format("Year and model: %s profit:%f",key, yearlyTrans.profit));
	            LogManager.write("yearlyMatrixModelOutput", String.format("%s,%s,%s\n", i++,key, yearlyTrans.profit));
	        }
	}

	final static String DATE_FORMAT = "dd-MM-yyyy";

	public static boolean isDateValid(String date) 
	{
	        try {
	            DateFormat df = new SimpleDateFormat(DATE_FORMAT);
	            df.setLenient(false);
	            df.parse(date);
	            return true;
	        } catch (ParseException e) {
	            return false;
	        } catch (java.text.ParseException e) {
				return false;
			}
	}
	private static void transform() {
		ExtractTransformLoadManager  m = null;
		m = new ExtractTransformLoadManager();
		Hashtable<String, Trade> t = m.transform();
		DailyTradeTransactionManager dtm = DailyTradeTransactionManager.getInstance();

		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
       		
		int startYear = Integer.parseInt(appProperties.getProperty("startYear"));
		int startMonth = Integer.parseInt(appProperties.getProperty("startMonth"));
		int startDay = Integer.parseInt(appProperties.getProperty("startDay"));

		int endYear = Integer.parseInt(appProperties.getProperty("endYear"));
		int endMonth = Integer.parseInt(appProperties.getProperty("endMonth"));
		int endDay = Integer.parseInt(appProperties.getProperty("endDay"));
		
		int tradeStartHour = Integer.parseInt(appProperties.getProperty("tradeStartHour"));
		int asianStartHour = Integer.parseInt(appProperties.getProperty("asianStartHour"));
		int startMinute = Integer.parseInt(appProperties.getProperty("startMinute"));
		int tradeEndHour = Integer.parseInt(appProperties.getProperty("tradeEndHour"));
		
		
		Hashtable<String , MatrixModel> mmTable = m.extractModel();
		
		String startProcessingIndexOfModel = appProperties.getProperty("startProcessingIndexOfModel");
		
		String endProcessingIndexOfModel = appProperties.getProperty("endProcessingIndexOfModel");
		MatrixModel mm = null;
		int end = Integer.parseInt(endProcessingIndexOfModel);
		int start = Integer.parseInt(startProcessingIndexOfModel);
		for(int i = start; i < end ;i++){
			mm = mmTable.get(String.valueOf(i));
		
			if(mm != null){
		
				processDateRange(t, dtm, startYear, startMonth, startDay, endYear,
				endMonth, endDay, tradeStartHour, asianStartHour, tradeEndHour,
				mm);
			}

		}
		
		processYearlyTransactions();		


		
	
		
	}
	private static void processDateRange(Hashtable<String, Trade> t,
			DailyTradeTransactionManager dtm, int startYear, int startMonth,
			int startDay, int endYear, int endMonth, int endDay,
			int tradeStartHour, int asianStartHour, int tradeEndHour,
			MatrixModel mm) {
		for(int year = startYear; year <= endYear; year++){
			for(int month = startMonth; month <= endMonth; month++){
				for(int day = startDay; day <= endDay; day++){
					//Date.parse(String.format("%s/%s/%s",month, day, year));
					//"dd-MM-yyyy"
					String dateValue = String.format("%s-%s-%s",day,month,year); 
					if(isDateValid(dateValue)){
						//System.out.println("date is good: " + dateValue);
						dtm.process(t, year, month, day, tradeStartHour, asianStartHour, tradeEndHour, mm);
					}
//					else{
//						System.out.println("date is not good :" + dateValue);
//					}
				}
			}
		
		}
	}

//	private static void createMatrix() throws IOException {
//		System.out.println("Create the Profit matrix model");
//		//
//		//pt1
//		FileWriter writer = new FileWriter("/Users/clydemoreno/Downloads/nadex/ProfitMatrixModel.csv");
//		
//		
//		PositionManager pm = new PositionManager();
//		Position p;
//		float size2 = 0;
//		ArrayList<Position> list = new ArrayList<Position>();
//		int id = 0;
//		List<Position> ll = new LinkedList<Position>();
//		for(int i = 0; i <= 100;i+=2){
//			for(int j = 0; j <= 100; j+=2){
//				for(int k = 0; k <= 100; k+=2){
//					if(i + j + k == 100){
//						for(int l = 0; l <= 100;l+=3){
//							for(int m = 50; m <= 150;m+=5){
//								
//								for(int n = 80; n <= 200; n+=10){
//								p = new Position();
//								//ll.add(p);
//								p.id = id++;
//								p.positionSize1 = i;
//								p.positionSize2 = j;
//								p.positionSize3 = k;
//								
//								p.profitTargetPercentage1 = l;
//								p.profitTargetPercentage2 = m;
//								p.profitTargetPercentage3 = n;
////								if(m > l){
////									p.profitTargetPercentage2 = m;
////								}
////								else{
////									p.profitTargetPercentage2 = l;
////								}
////								if(n > m){
////								p.profitTargetPercentage3 = n;
////								}
////								else{
////									p.profitTargetPercentage3 = l;
////								}
//								
////								if(l == 0){
////									p.profitTargetPercentage1 = m;
////								}
////								else if(l == 1){
////									p.profitTargetPercentage2 = m;
////								}
////								else if(l == 2){
////									p.profitTargetPercentage3 = m;
////								}
//								
//								writer.append(pm.toString(p));
//								//pm.display(p);
//								
//								}
//							}
//						}
//					}
//				}
//			}
//				
//		}
//		//Position pp = list.get(50);
//		System.out.println(String.format("Done. Total items: %d\n", id));
////		for(int i = 100;i > 0;i--){
////			//for(int j = 100; j >= 0;j--){
////			    size2 = 100 - i;
////				for(int j = 0; j < size2;size2++){
////					p = new Position();
////					p.positionSize1 = i;
////					p.positionSize2 = j;
////					size2 = p.positionSize2;
////					p.positionSize3 = 100 - (p.positionSize1 + p.positionSize2);
////					pm.display(p);
////				}
////				//p.positionSize3 = 100 - (p.positionSize2 + p.positionSize1);
////								
////			//}
////
////		}
//	}

}
