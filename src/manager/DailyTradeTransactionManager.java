package manager;

import java.util.Hashtable;
import java.util.List;

import utility.Calculator;

import model.Account;
import model.DailyTradeTransaction;
import model.MatrixModel;
import model.Trade;

public class DailyTradeTransactionManager {

	private static DailyTradeTransactionManager instance;
	
	public static DailyTradeTransactionManager getInstance()
	{
		if(instance==null)
		{
			instance=new DailyTradeTransactionManager();
		}
		return instance;
	}
	
	public void process(Hashtable<String, Trade> t,int year,int month,int day,int tradeStartHour, int asianStartHour,int tradeEndHour,MatrixModel model)
	{
		//DailyTradeTransaction dt = new DailyTradeTransaction();
		float high=0,low=Float.MAX_VALUE,lots=0;
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
        float accountBalance = Float.parseFloat(appProperties.getProperty("accountBalance"));
        float accountAverageStopLoss = Float.parseFloat(appProperties.getProperty("accountAverageStopLoss"));
        float accountRiskPercentage = Float.parseFloat(appProperties.getProperty("accountRiskPercentage"));
		
		Account account = new Account();
		account.balance=accountBalance;
		account.averageStopLoss=accountAverageStopLoss;
		account.riskPercentage=accountRiskPercentage;
		for(int i = 0; i < 3; i++){
			account.setProfitTargetPercentage(i,model.getProfitTargetPercentage(i));
			account.setProfitTargetLotPercentage(i ,model.getProfitTargetLotPercentage(i));
		}
		
		AccountManager accountManager = AccountManager.getInstance();
		lots=accountManager.getLotSize(account);
		
		for(int i = asianStartHour; i < tradeStartHour; i++)
		{
			for(int min=0; min<60;min++){
//				System.out.println(String.format("hour:%d min: %02d",i, min));
//				System.out.println(String.format("%d.%02d.%02d,%02d:%02d", year,month,day,i,min));
				Trade trade = (Trade)t.get(String.format("%d.%02d.%02d,%02d:%02d", year,month,day,i,min));
				if(trade!=null){
//					System.out.println(String.format("DateTime:%s", trade.id));
					if(trade.high>high)	high=trade.high;
					if(trade.low<low) low=trade.low;
				}
				else{
					break;
				}
			}
		}
		if(high == 0 && low == Float.MAX_VALUE) return;
		
		//System.out.println(String.format("High:%s Low:%s PT1:%s PT2:%s PT3:%s", high, low ,model.getProfitTargetPercentage(0) ,model.getProfitTargetPercentage(1),model.getProfitTargetPercentage(2)   ));
		
		
		
		
		for(int i=0;i<3;i++)
		{
			DailyTradeTransaction buyEntry = new DailyTradeTransaction();
			DailyTradeTransaction sellEntry = new DailyTradeTransaction();
			
			buyEntry.profitTartgetPercentage = account.getProfitTargetPercentage(i);
			buyEntry.entry=high;
			buyEntry.type="Buy";
			
			buyEntry.lotSize = (lots * account.getProfitTargetLotPercentage(i));
			buyEntry.lotSizePercentage = account.getProfitTargetPercentage(i);
			buyEntry.profitTarget=Calculator.getProfitTarget(buyEntry.type, high, low, buyEntry.profitTartgetPercentage);
			buyEntry.stopLoss = Calculator.getStopLoss(high, low);
			buyEntry.isEntryPriceGotHit=false;
			buyEntry.active=true;
			buyEntry.isProfitTargetHit=false;
			buyEntry.isStopLossGotHit=false;
			
			sellEntry.profitTarget=Calculator.getProfitTarget(buyEntry.type, high, low, buyEntry.profitTartgetPercentage);
			sellEntry.profitTartgetPercentage = account.getProfitTargetPercentage(i);
			sellEntry.entry=low;
			sellEntry.type="Sell";
			sellEntry.lotSize = (lots * account.getProfitTargetLotPercentage(i));
			sellEntry.lotSizePercentage=account.getProfitTargetLotPercentage(i);
			sellEntry.profitTarget= Calculator.getProfitTarget(sellEntry.type, high, low, buyEntry.profitTartgetPercentage);
			sellEntry.stopLoss= Calculator.getStopLoss(high, low);
			sellEntry.isEntryPriceGotHit=false;
			sellEntry.active=true;
			sellEntry.isProfitTargetHit=false;
			sellEntry.isStopLossGotHit=false;
			account.buyList.add(buyEntry);
			account.sellList.add(sellEntry);
		}
//		for(DailyTradeTransaction buyDailyTradeTransaction:account.buyList)
//		{
//			System.out.println(String.format("Buy Entry:%s StopLoss:%s ProfitTarget:%s ProfitPercentage:%s Lots:%s LotsPercentage:%s", buyDailyTradeTransaction.entry,buyDailyTradeTransaction.stopLoss,buyDailyTradeTransaction.profitTarget,buyDailyTradeTransaction.profitTartgetPercentage, buyDailyTradeTransaction.lotSize, buyDailyTradeTransaction.lotSizePercentage));
//				
//		}
//		for(DailyTradeTransaction sellDailyTradeTransaction:account.sellList)
//		{
//			System.out.println(String.format("Sell Entry:%s StopLoss:%s ProfitTarget:%s ProfitPercentage:%s Lots:%s LotsPercentage:%s", sellDailyTradeTransaction.entry,sellDailyTradeTransaction.stopLoss,sellDailyTradeTransaction.profitTarget,sellDailyTradeTransaction.profitTartgetPercentage, sellDailyTradeTransaction.lotSize, sellDailyTradeTransaction.lotSizePercentage));
//				
//		}
		monitorTrades(account,t, year, month, day, tradeStartHour, tradeEndHour,model);
	}
	
	private void monitorTrades(Account account,Hashtable<String,Trade> t,int year,int month,int day,int tradeStartHour,int tradeEndHour, MatrixModel model)
	{
		String modelMatrix = String.format("%s-%s-%s|%s-%s-%s",model.getProfitTargetLotPercentage(0)
				,model.getProfitTargetLotPercentage(1)
				,model.getProfitTargetLotPercentage(2)
				,model.getProfitTargetPercentage(0)
				,model.getProfitTargetPercentage(1)
				,model.getProfitTargetPercentage(2));
		float lastClosePrice=0;
		for(int i = tradeStartHour; i < tradeEndHour; i++)
		{
			for(int min=0; min<60;min++){
				Trade trade = (Trade)t.get(String.format("%d.%02d.%02d,%02d:%02d", year,month,day,i,min));
				if(trade!=null){
					lastClosePrice=trade.close;
					for(DailyTradeTransaction buyDailyTradeTransaction:account.buyList)
					{
						if(buyDailyTradeTransaction.isEntryPriceGotHit==false && buyDailyTradeTransaction.active==true)
						{
							if(trade.open>=buyDailyTradeTransaction.entry || trade.high>=buyDailyTradeTransaction.entry)
							{
								buyDailyTradeTransaction.isEntryPriceGotHit=true;
								buyDailyTradeTransaction.entryTime = trade.id;
							}
						}
						if(buyDailyTradeTransaction.isEntryPriceGotHit==true && buyDailyTradeTransaction.active==true)
						{
							if(trade.open<=buyDailyTradeTransaction.stopLoss || trade.low<=buyDailyTradeTransaction.stopLoss)
							{
								buyDailyTradeTransaction.isStopLossGotHit=true;
								buyDailyTradeTransaction.exitTime = trade.id;
								buyDailyTradeTransaction.active=false;
								buyDailyTradeTransaction.profit = Calculator.calculateProfit(buyDailyTradeTransaction);
								LogManager.write(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",buyDailyTradeTransaction.entryTime, buyDailyTradeTransaction.type , buyDailyTradeTransaction.profit,  buyDailyTradeTransaction.entry,buyDailyTradeTransaction.stopLoss,buyDailyTradeTransaction.profitTarget
										,buyDailyTradeTransaction.profitTartgetPercentage, buyDailyTradeTransaction.lotSize, buyDailyTradeTransaction.lotSizePercentage, modelMatrix));
								
								//profitarget-entry=3.5*10=35 35*11.99
							}
							if(buyDailyTradeTransaction.active && (trade.open>=buyDailyTradeTransaction.profitTarget || trade.high>=buyDailyTradeTransaction.profitTarget))
							{
								buyDailyTradeTransaction.isProfitTargetHit=true;
								buyDailyTradeTransaction.exitTime = trade.id;
								buyDailyTradeTransaction.active=false;
								buyDailyTradeTransaction.profit=((buyDailyTradeTransaction.profitTarget-buyDailyTradeTransaction.entry))*buyDailyTradeTransaction.lotSize;
								LogManager.write(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", buyDailyTradeTransaction.entryTime, buyDailyTradeTransaction.type, buyDailyTradeTransaction.profit, buyDailyTradeTransaction.entry,buyDailyTradeTransaction.stopLoss,buyDailyTradeTransaction.profitTarget
										,buyDailyTradeTransaction.profitTartgetPercentage, buyDailyTradeTransaction.lotSize, buyDailyTradeTransaction.lotSizePercentage, modelMatrix));
								disableActiveTradesWhenOtherTradeIsSuccessful(account.sellList);
							}
						}
						//check stoploss is hit
					}
					
					for(DailyTradeTransaction sellDailyTradeTransaction:account.sellList)
					{
						if(sellDailyTradeTransaction.isEntryPriceGotHit==false && sellDailyTradeTransaction.active==true)
						{
							if(trade.open<=sellDailyTradeTransaction.entry || trade.low<=sellDailyTradeTransaction.entry)
							{
								sellDailyTradeTransaction.isEntryPriceGotHit=true;
								sellDailyTradeTransaction.entryTime = trade.id;
							}
						}
						if(sellDailyTradeTransaction.isEntryPriceGotHit==true && sellDailyTradeTransaction.active==true)
						{
							if(trade.open>=sellDailyTradeTransaction.stopLoss || trade.high>=sellDailyTradeTransaction.stopLoss)
							{
								sellDailyTradeTransaction.isStopLossGotHit=true;
								sellDailyTradeTransaction.active=false;
								sellDailyTradeTransaction.exitTime = trade.id;
								sellDailyTradeTransaction.profit=((sellDailyTradeTransaction.entry - sellDailyTradeTransaction.stopLoss)) * sellDailyTradeTransaction.lotSize;
								//profitarget-entry=3.5*10=35 35*11.99
								LogManager.write(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n", sellDailyTradeTransaction.entryTime, sellDailyTradeTransaction.type, sellDailyTradeTransaction.profit, sellDailyTradeTransaction.entry
										,sellDailyTradeTransaction.stopLoss,sellDailyTradeTransaction.profitTarget
										,sellDailyTradeTransaction.profitTartgetPercentage, sellDailyTradeTransaction.lotSize
										, sellDailyTradeTransaction.lotSizePercentage, modelMatrix));
							}
							if(sellDailyTradeTransaction.active && (trade.open<=sellDailyTradeTransaction.profitTarget || trade.low<=sellDailyTradeTransaction.profitTarget))
							{
								sellDailyTradeTransaction.isProfitTargetHit=true;
								sellDailyTradeTransaction.exitTime = trade.id;
								sellDailyTradeTransaction.active=false;
								sellDailyTradeTransaction.profit=((sellDailyTradeTransaction.entry-sellDailyTradeTransaction.profitTarget))*sellDailyTradeTransaction.lotSize;
								disableActiveTradesWhenOtherTradeIsSuccessful(account.buyList);
								LogManager.write(String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n",sellDailyTradeTransaction.entryTime,  sellDailyTradeTransaction.type, sellDailyTradeTransaction.profit,  sellDailyTradeTransaction.entry,sellDailyTradeTransaction.stopLoss
										,sellDailyTradeTransaction.profitTarget,sellDailyTradeTransaction.profitTartgetPercentage, sellDailyTradeTransaction.lotSize
										, sellDailyTradeTransaction.lotSizePercentage, modelMatrix));
							}
						}
						//check stoploss is hit
					}
						//if(trade.open>=)	high=trade.high;
				//	if(trade.low<low) low=trade.low;
				}
			}
		}
		
		handleEndOfTradingSession(account, lastClosePrice);
		
		
		
	}



	private void handleEndOfTradingSession(Account account, float lastClosePrice) {
		for(DailyTradeTransaction buyDailyTradeTransaction:account.buyList)
		{
//			System.out.println(String.format("Buy Entry Time: %s Exit Time: %s Entry Price:%s StopLoss:%s ProfitTarget:%s ProfitPercentage:%s Lots:%s LotsPercentage:%s Profit:%s",buyDailyTradeTransaction.entryTime,buyDailyTradeTransaction.exitTime, buyDailyTradeTransaction.entry,buyDailyTradeTransaction.stopLoss,buyDailyTradeTransaction.profitTarget,buyDailyTradeTransaction.profitTartgetPercentage, buyDailyTradeTransaction.lotSize, buyDailyTradeTransaction.lotSizePercentage,buyDailyTradeTransaction.profit));
			if(buyDailyTradeTransaction.active && buyDailyTradeTransaction.isStopLossGotHit==false && buyDailyTradeTransaction.isProfitTargetHit==false && buyDailyTradeTransaction.isEntryPriceGotHit)
			{
				buyDailyTradeTransaction.profit=lastClosePrice-buyDailyTradeTransaction.entry;
				buyDailyTradeTransaction.active=false;
			}
			if(buyDailyTradeTransaction.active==false && buyDailyTradeTransaction.isEntryPriceGotHit==false)
			{
				buyDailyTradeTransaction.active=false;
			}
		}
	
		for(DailyTradeTransaction sellDailyTradeTransaction:account.sellList)
		{
			
//			System.out.println(String.format("Sell Entry Time: %s Exit Time: %s Entry Price:%s StopLoss:%s ProfitTarget:%s ProfitPercentage:%s Lots:%s LotsPercentage:%s Profit:%s",sellDailyTradeTransaction.entryTime, sellDailyTradeTransaction.exitTime, sellDailyTradeTransaction.entry,sellDailyTradeTransaction.stopLoss,sellDailyTradeTransaction.profitTarget,sellDailyTradeTransaction.profitTartgetPercentage, sellDailyTradeTransaction.lotSize, sellDailyTradeTransaction.lotSizePercentage,sellDailyTradeTransaction.profit));
			if(sellDailyTradeTransaction.active && sellDailyTradeTransaction.isStopLossGotHit==false && sellDailyTradeTransaction.isProfitTargetHit==false && sellDailyTradeTransaction.isEntryPriceGotHit)
			{
				sellDailyTradeTransaction.profit=sellDailyTradeTransaction.entry-lastClosePrice;
				sellDailyTradeTransaction.active=false;
			}
			if(sellDailyTradeTransaction.active==false && sellDailyTradeTransaction.isEntryPriceGotHit==false)
			{
				sellDailyTradeTransaction.active=false;
			}
		}
	}
	
//	public float getProfitTarget(String type, float high, float low, float percentage)
//	{
//		if(type=="Buy")	
//			return high+((high-low)*percentage);
//		else	
//			return low-((high-low)*percentage);
//	}
	
//	public float getStopLoss(float high, float low)
//	{
//		return high-((high-low)/2);
//	}
	
	public void disableActiveTradesWhenOtherTradeIsSuccessful(List<DailyTradeTransaction> tradeList)
	{
		for(DailyTradeTransaction tradeTransaction:tradeList)
		{
			tradeTransaction.active=false;
		}
	}
}
