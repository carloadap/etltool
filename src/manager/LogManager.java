package manager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class LogManager {
	public static void clearLogs(String propertyName){
		AppProperties appProperties = AppProperties.getInstance();
        appProperties.load();
		String outputFilePath = appProperties.getProperty(propertyName);
		String clearLog = appProperties.getProperty("clearLog");
	  
		File file = new File(outputFilePath);

		// if file doesnt exists, then create it
		//if (!file.exists()) {
		if(clearLog.equals("true")){
		   file.delete();
		}
			
		//}
		
	}
	public static void write(String line){
		write("",line);
	}	
	public static void write(String pathfileName,String line){
		try {
			String outputFilePath = "";
			AppProperties appProperties = AppProperties.getInstance();
	        appProperties.load();
	        if(!pathfileName.equals("")){
	        	outputFilePath = appProperties.getProperty(pathfileName);
	        }
	        else{
	        	outputFilePath = appProperties.getProperty("outputFilePath");
	        }
		  
			File file = new File(outputFilePath);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile(),true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.append(line);
			bw.flush();
			bw.close();
 
//			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
